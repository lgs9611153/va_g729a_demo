# va_g729a_demo

#### 介绍
g729a编解码例程，g729a与pcm相互转换。转换后的音质不咋地。


#### 安装教程

vc2005编译。

#### 使用说明

1.  va_g729a_encoder sound1.pcm sound2.wav

2.  va_g729a_decoder sound2.wav sound3.pcm

#### 参与贡献

内容来自网络，非原创。

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
